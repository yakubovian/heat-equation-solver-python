# -*- coding: utf-8 -*-

#   Программа предназначена для численного решения трехмерного нестационарного уравнения теплопроводности с коэффициентом теплопроводности, зависящим от температуры
#   Размерность физических единиц соответсвует системе СИ


import numpy as np
import matplotlib.pyplot as plt

import sys
import solvers
from solvers import conductivity, TDMA

class Solver:
    #   Набор функций для решения уравнения

    #   Конструктор
    def __init__(self, media, pump):
        #   media - экземпляр класса Media
    
        self.media = media
        self.pump = pump

        
    #   Генерация пространственно-временной сетки
    def grid(self, tmin=0, tmax=10, nt=50, nx=40, nz=20):
        #   tmin - начальный момент времени
        #   tmax - конечный момент времени
        #   nt - количество точек по времени
        #   nx - количество точек в поперечном направлении относительно оси накачки
        #   nz - количество точек в продольном направлении относительно оси накачки
    
        self.nt, self.nx, self.nz = nt, nx, nz
        self.x, self.dx = np.linspace(-self.media.diameter*0.5, self.media.diameter*0.5, nx, retstep=1)
        self.z, self.dz = np.linspace(0, self.media.length, nz, retstep=1)
        self.t, self.dt = np.linspace(tmin, tmax, nt, retstep=1)
        self.dt = self.dt / 3.
        self.delta = self.media.cp * self.media.ro / self.dt
        print( u'Расчетная область сгенерирована c параметрами: tmax=%.1f, nt=%d, nx=%d, nz=%d' % (tmax, nt, nx, nz) )
        
    
    #   Определение источника нагрева
    def source(self, losses=1-0.94/1.03):
        #   losses - часть энергии накачки, переходящая в тепло
        
        self.Q = np.zeros((self.nx, self.nx, self.nz))
        self.P = np.zeros((self.nx, self.nx, self.nz))
        distrib = np.zeros((self.nx, self.nx))
        shape = self.pump.shape
        radius = self.pump.radius
        power = self.pump.power
        gamma = self.media.concentration * self.media.absorp_p

        for xx in range(self.nx):
            for yy in range(self.nx):
                
                if shape == 'hyper':
                    distrib[xx,yy] = np.exp(-2 * ((self.x[xx]**2 + self.x[yy]**2) / radius**2)**3)
                elif shape == 'gaussian':
                    distrib[xx,yy] = np.exp(-2 * (self.x[xx]**2 + self.x[yy]**2) / radius**2)
                elif shape == 'fwhm':
                    distrib[xx,yy] = np.exp(-np.log(2) * (self.x[xx]**2 + self.x[yy]**2) / (radius**2))
                else:
                    print(u'Неизвестный формат пространственного профиля!')
                    raise NameError
        normalization = np.sum(distrib) * self.dx**2
        for xx in range(self.nx):
            for yy in range(self.nx):
                self.P[xx,yy,:] = power * np.exp(-gamma*self.z) * distrib[xx,yy] / normalization
                self.Q[xx,yy,:] = self.P[xx,yy,:] * losses * gamma
                
        convergence = np.sum(self.Q) * self.dx * self.dx * self.dz / (power * losses * (1 - np.exp(-gamma * self.media.length)))
        print(u'Мощность источника: %.1f Вт, радиус: %.1f cм, профиль: %s, длительность: %s' % (power, radius, shape, str(self.pump.tp)))
        print(u'Проверка сходимости интегральной нормированной мощности источника: %.5f' % convergence)
        
        if self.pump.tp == 'continuous':
            self.driver = lambda time: 1
        else:
            def driver(self, time):
                if abs(time) <= self.pump.tp:
                    return 1
                else:
                    return 0


    #   Решение уравнения для всех точек области
    def solver(self, initial='room', bounds={ 'x':{'Tleft':300., 'Tright':300., 'flowLeft':False, 'flowRight':False},
                                              'y':{'Tleft':300., 'Tright':300., 'flowLeft':False, 'flowRight':False},
                                              'z':{'Tleft':False, 'Tright':False, 'flowLeft':1., 'flowRight':1.} }, timeLog=False):
        #   initial - начальное условие для функции температуры, два ключа - 'room'/'cryogenic'
        #   bounds - граничные условия для каждой из осей x,y,z, передаются в формате 
        #   {ось : {температура на левой границе:значение, температура на правой границе:значение, поток на левой границе:значение, поток на правой границе:значение},...}
        #   для границы может использоваться только один тип условий, поэтому для другого типа должно передаваться условие False
        #   timeLog - массив с тремя индексами для записи динамики нагрева в заданной точке
        
        for axis in bounds.keys():
            if bounds[axis]['flowLeft'] == False:
                if bounds[axis]['flowRight'] == False:
                    print(u'Для оси %s заданы условия: температура на левой границе %.1f, температура на правой границе %.1f' % 
                                            (axis, bounds[axis]['Tleft'], bounds[axis]['Tright']))
                else:
                    print(u'Для оси %s заданы условия: температура на левой границе %.1f, поток на правой границе %.1f' % 
                                            (axis, bounds[axis]['Tleft'], bounds[axis]['flowLeft']))
            else:
                if bounds[axis]['flowRight'] == False:
                    print(u'Для оси %s заданы условия: поток на левой границе %.1f, температура на правой границе %.1f' % 
                                            (axis, bounds[axis]['flowLeft'], bounds[axis]['Tright']))
                else:
                    print(u'Для оси %s заданы условия: поток на левой границе %.1f, поток на правой границе %.1f' % 
                                            (axis, bounds[axis]['flowLeft'], bounds[axis]['flowLeft']))
        if initial == 'room':
            self.temperature = np.full((self.nx,self.nx,self.nz), 300.)
            print(u'Среда при комнатной температуре')
        else:
            self.temperature = np.full((self.nx,self.nx,self.nz), 100.)
            print(u'Среда при криогенной температуре')
            
        if timeLog == True:
            self.dynamic = np.full(self.nt, 300. if initial == 'room' else 100.)
            print(u'Режим записи динамики нагрева')


        print(u'Расчет начат')
        for tt in range(self.nt):

            # # x-ось
            Tleft, Tright = bounds['x']['Tleft'], bounds['x']['Tright']
            flowLeft, flowRight = bounds['x']['flowLeft'], bounds['x']['flowRight']
            for zz in range(self.nz):
                for yy in range(self.nx):
                    self.temperature[:, yy, zz] = TDMA(points=self.nx, dh=self.dx, delta=self.delta,
                                                       temperature=self.temperature[:, yy, zz],
                                                       Q=self.Q[:, yy, zz],
                                                       Tleft=Tleft, Tright=Tright,
                                                       flowLeft=flowLeft, flowRight=flowRight)
            # y-ось
            Tleft, Tright = bounds['y']['Tleft'], bounds['y']['Tright']
            flowLeft, flowRight = bounds['y']['flowLeft'], bounds['y']['flowRight']
            for zz in range(self.nz):
                for xx in range(self.nx):
                    self.temperature[xx, :, zz] = TDMA(points=self.nx, dh=self.dx, delta=self.delta,
                                                       temperature=self.temperature[xx, :, zz],
                                                       Q=self.Q[xx, :, zz],
                                                       Tleft=Tleft, Tright=Tright,
                                                       flowLeft=flowLeft, flowRight=flowRight)
            # z-ось
            Tleft, Tright = bounds['z']['Tleft'], bounds['z']['Tright']
            flowLeft, flowRight = bounds['z']['flowLeft'], bounds['z']['flowRight']
            for xx in range(self.nx):
                for yy in range(self.nx):
                    self.temperature[xx, yy, :] = TDMA(points=self.nz, dh=self.dz, delta=self.delta,
                                                       temperature=self.temperature[xx, yy, :],
                                                       Q=self.Q[xx, yy, :],
                                                       Tleft=Tleft, Tright=Tright,
                                                       flowLeft=flowLeft, flowRight=flowRight)


            if timeLog == True:
                self.dynamic[tt] = self.temperature[self.nx/2,self.nx/2,0]

            sys.stdout.write(u'Выполнено: %.1f %%' % (100. * (tt + 1) / self.nt))
            sys.stdout.write('\r')
        print(u'Расчет окончен')
        
    #   Визуализация результатов моделирования
    def show(self, key='steady'):
        #   key - 'dynamic'/'steady'


        plt.rc('font', family='Verdana')
        
        if key == 'dynamic':
            plt.plot(self.t, self.dynamic, label=u'Динамика нагрева активного элемента')
            plt.xlabel(u'Время, с')
            plt.ylabel(u'Температура, К')
            plt.grid()
            plt.legend(loc=4)
            plt.show()
        else:
            plt.imshow(self.temperature[:,:,0])
            plt.colorbar()
            plt.show()


if __name__ == '__main__':

    media = solvers.Media(length=0.2)
    pump = solvers.Pump(radius=0.5e-1, power=50)
    solver = Solver(media=media, pump=pump)
    solver.grid(tmax=5, nt=60, nx=60, nz=60)
    solver.source()
    solver.solver(initial='cryo', bounds={ 'x':{'Tleft':False, 'Tright':False, 'flowLeft':1., 'flowRight':1.},
                        'y':{'Tleft':False, 'Tright':False, 'flowLeft':1., 'flowRight':1.},
                        'z':{'Tleft':False, 'Tright':120., 'flowLeft':1., 'flowRight':False}})
    solver.show('steady')

