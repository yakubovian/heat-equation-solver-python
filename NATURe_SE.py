# -*- coding: utf-8 -*-

#   Программа предназначена для численного решения трехмерного нестационарного уравнения теплопроводности с коэффициентом теплопроводности, зависящим от температуры
#   Размерность физических единиц соответсвует системе СИ

import sys
import numpy as np
import matplotlib.pyplot as plt
import solvers
from solvers import conductivity, TDMA, Inversion, Square

class Heat:
    #   Набор функций для решения уравнения

    #   Конструктор
    def __init__(self, media, pump, verbose=False):
        #   media - экземпляр класса Media
        #   pump - кземляр класса Pump
    
        self.media = media
        self.pump = pump
        self.verbose = verbose

        
    #   Генерация пространственно-временной сетки
    #   Количество точек на элемент однинаковое
    def grid(self, tmin=0, tmax=10, nt=50, nx=40, nz=20):
        #   tmin - начальный момент времени
        #   tmax - конечный момент времени
        #   nt - количество точек по времени
        #   nx - количество точек в поперечном направлении относительно оси накачки
        #   nz - количество точек в продольном направлении относительно оси накачки
    
        self.nt, self.nx, self.nz = nt, nx, nz
        self.t, self.dt = np.linspace(tmin, tmax, nt, retstep=1)
        self.dt /= 3.
        self.x, self.dx = np.linspace(-self.media.diameter * 0.5, self.media.diameter * 0.5, nx, retstep=1)
        self.z, self.dz = np.linspace(0, self.media.length, nz, retstep=1)
        self.delta = self.media.cp * self.media.ro / self.dt
        if self.verbose:
            print( u'Расчетная область сгенерирована c параметрами: tmax=%.1f, nt=%d, nx=%d, nz=%d' % (tmax, nt, nx, nz) )
        
    
    #   Определение источника нагрева
    def source(self):
        #   losses - часть энергии накачки, переходящая в тепло
        
        self.Q = np.zeros((self.nx, self.nx, self.nz))
        self.P = np.zeros((self.nx, self.nx, self.nz))
        self.pump.grid(x=self.x, dx=self.dx, nx=self.nx, verbose=self.verbose)

    def cryomech(self, P, links=True):

        if links:
            return 38.3 + 0.18 * P + 5.88e-3 * P ** 2
        else:
            P *= 0.5
            return 38.3 + 0.18 * P + 5.88e-3 * P ** 2


    def phase_aberrations(self, timepoint, where='space'):

        #   Фаза во времени
        thermal_average = np.zeros((self.nx, self.nx, self.nz))
        if where == 'time':
            for xx in range(self.nx):
                for yy in range(self.nx):
                    if abs(self.x[xx]) < self.pump.radius and abs(self.x[yy]) < self.pump.radius:
                        for zz in range(self.nz):
                            thermal_average[xx, yy, zz] = self.temperature[xx, yy, zz] - self.temperature[self.nx - 1, self.nx - 1, zz]
            self.thermalPhaseTime[timepoint] = np.sum(thermal_average) * self.average_coefficient

        #   Фаза в пространстве
        elif where == 'space':
            self.thermalPhaseSpatial = np.zeros((self.nx, self.nx))
            for xx in range(self.nx):
                for yy in range(self.nx):
                    temperature_difference = self.temperature[xx, yy, :self.nz] - self.temperature[self.nx - 1, self.nx - 1, :self.nz]
                    self.thermalPhaseSpatial[xx, yy] = np.sum(temperature_difference) * self.average_coefficient


    #   Решение уравнения для всех точек области
    def solver(self, initial='cryogenic', cooling_axis='z', links=True, timeLog=False, aberrations=False):
        #   initial :: str - начальное условие для функции температуры, два ключа - 'room'/'cryogenic'
        #   cooling_axis :: str - ось, вдоль которой происходит охлаждение; 'x'/'y'/'z'
        #   links :: bool - охлаждение с линками или без
        #   timeLog :: bool
        #   aberrations :: str - расчет тепловой составляющей фазовых искажений; 'time' / 'space'

        if initial == 'room':
            self.temperature = np.full((self.nx, self.nx, self.nz), 300.)
            print(u'Среда при комнатной температуре')
        else:
            self.temperature = np.full((self.nx, self.nx, self.nz), 90.)
            print(u'Среда при криогенной температуре')

        if timeLog:
            self.dynamic = np.full(self.nt, 300. if initial == 'room' else 90.)
            print(u'Режим записи динамики нагрева')

        if aberrations:
            if aberrations == 'time':
                self.thermalPhaseTime = np.zeros(self.nt)
                self.average_coefficient = self.dz * self.dx ** 2 / ((2 * self.pump.radius) ** 2) * self.media.xi / self.media.ls
            elif aberrations == 'space':
                self.average_coefficient = self.dz * self.media.xi / self.media.ls
                self.thermalPhaseSpatial = np.zeros((self.nx, self.nx))

        n = np.zeros((self.nx, self.nx, self.nz))
        conc = self.media.concentration
        hv = self.media.h * self.media.freqp
        life = self.media.lifetime
        gamma = np.zeros(self.nz)
        losses = self.media.losses
        pump_points = self.pump.points_per_pumping
        pumping_time = self.pump.pumping_time
        period = self.pump.period
        pulse_duration = self.pump.tp
        repetition_rate_coeff = min(1, pulse_duration / period)
        # self.media.bounds[cooling_axis]['Tright'] = self.cryomech(P=self.pump.power * repetition_rate_coeff, links=links)

        print(u'Расчет начат')
        for tt in range(self.nt):

            for xx in range(self.nx):
                for yy in range(self.nx):
                    for zz in range(self.nz):
                        sigma = solvers.ACS(T=self.temperature[xx, yy, zz])
                        amplitude = self.P[xx, yy, zz] * sigma / hv
                        f01 = solvers.lower_manifold_pump(T=self.temperature[xx, yy, zz])
                        f12 = solvers.upper_manifold_pump(T=self.temperature[xx, yy, zz])
                        n[xx, yy, zz] = Inversion(points=pump_points, x0=0, xn=pumping_time, init=0,
                                                         amplitude_main=amplitude, concentration=conc, lifetime=life,
                                                         f01=f01, f12=f12, period=period, pulse_duration=pulse_duration)
                        gamma[zz] = (conc - n[xx, yy, zz]) * sigma * f01
                        self.P[xx, yy, zz] = self.pump.power_distribution[xx, yy] * np.exp(-sum(gamma[0:zz]) * self.dz) * repetition_rate_coeff
                    self.Q[xx, yy, 0:self.nz - 1] = - np.diff(self.P[xx, yy, 0:self.nz]) / self.dz * losses / 3.

            # x-ось
            Tleft, Tright = self.media.bounds['x']['Tleft'], self.media.bounds['x']['Tright']
            flowLeft, flowRight = self.media.bounds['x']['flowLeft'], self.media.bounds['x']['flowRight']
            for zz in range(self.nz):
                for yy in range(self.nx):
                    self.temperature[:, yy, zz] = TDMA(points=self.nx, dh=self.dx, delta=self.delta,
                                                       temperature=self.temperature[:, yy, zz],
                                                       Q=self.Q[:, yy, zz], material=0,
                                                       Tleft=Tleft, Tright=Tright,
                                                       flowLeft=flowLeft, flowRight=flowRight)
            # y-ось
            Tleft, Tright = self.media.bounds['y']['Tleft'],self.media.bounds['y']['Tright']
            flowLeft, flowRight = self.media.bounds['y']['flowLeft'], self.media.bounds['y']['flowRight']
            for zz in range(self.nz):
                for xx in range(self.nx):
                    self.temperature[xx, :, zz] = TDMA(points=self.nx, dh=self.dx, delta=self.delta,
                                                       temperature=self.temperature[xx, :, zz],
                                                       Q=self.Q[xx, :, zz], material=0,
                                                       Tleft=Tleft, Tright=Tright,
                                                       flowLeft=flowLeft, flowRight=flowRight)
            # z-ось
            Tleft, Tright = self.media.bounds['z']['Tleft'], self.media.bounds['z']['Tright']
            flowLeft, flowRight = self.media.bounds['z']['flowLeft'], self.media.bounds['z']['flowRight']
            for xx in range(self.nx):
                for yy in range(self.nx):
                    self.temperature[xx, yy, :] = TDMA(points=self.nz, dh=self.nz, delta=self.delta,
                                                       temperature=self.temperature[xx, yy, :],
                                                       Q=self.Q[xx, yy, :], material=0,
                                                       Tleft=Tleft, Tright=Tright,
                                                       flowLeft=flowLeft, flowRight=flowRight)
            if timeLog:
                self.dynamic[tt] = self.temperature[self.nx/2, self.nx/2, 0]

            if aberrations == 'time':
                self.phase_aberrations(timepoint=tt, where=aberrations)

            sys.stdout.write(u'Выполнено: %.1f %%' % (100. * (tt + 1) / self.nt))
            sys.stdout.write('\r')

        if aberrations == 'space':
            self.phase_aberrations(timepoint=tt, where=aberrations)

        print(u'Расчет окончен')
        
    #   Визуализация результатов моделирования
    def show(self, key='steady'):
        #   key - 'dynamic'/'steady'


        plt.rc('font', family='Verdana')
        
        if key == 'dynamic':
            plt.plot(self.t, self.dynamic, label=u'Динамика нагрева активного элемента')
            plt.xlabel(u'Время, с')
            plt.ylabel(u'Температура, К')
            plt.grid()
            plt.legend(loc=4)
            plt.show()
        else:
            plt.imshow(self.temperature[:,:,0])
            plt.colorbar()
            plt.show()


if __name__ == '__main__':

    crystal = solvers.Media(length=0.2, diameter=0.5, concentration=5)
    pump = solvers.Pump(radius=0.3e-1, power=100, tp=0.5e-3, period=1e-3, points_per_pumping=250, pumping_time=1.1e-3)
    insular = 1e-9
    bound = 300.
    crystal.Bounds({ 'x':{'Tleft':bound, 'Tright':bound, 'flowLeft':False, 'flowRight':False},
                    'y':{'Tleft':bound, 'Tright':bound, 'flowLeft':False, 'flowRight':False},
                    'z':{'Tleft':False, 'Tright':False, 'flowLeft':insular, 'flowRight':insular}})

    solver = Heat(media=crystal, pump=pump)
    solver.grid(tmax=100, nt=100, nx=30, nz=30)
    solver.source()
    solver.solver(initial='room', cooling_axis='z', links=True, timeLog=False, aberrations=False)
    # axis = [0, crystal.length + cooler.length, solver.x[0], solver.x[-1]]
    # plt.imshow(solver.temperature[:, solver.nx/2, :], extent=axis)
    # plt.colorbar()
    # plt.show()
    plt.plot(solver.temperature[int(solver.nx/2), int(solver.nx/2)])
    plt.show()


