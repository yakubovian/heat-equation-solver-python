# -*- coding: utf-8 -*-


import numpy as np
import matplotlib.pyplot as plt
import numba



def transport():

    for xx in range(nx - 1):
        for tt in range(nt - 1):
            a = u[tt + 1, xx] * 0.5 * (1. / dt - c / dx)
            b = u[tt, xx + 1] * 0.5 * (c / dx - 1. / dt)
            d = u[tt, xx] * 0.5 * (- 1. / dt - c / dx)
            e = 1e0 * u[tt, xx]
            # e = 0.5 * (u[tt + 1, xx] + u[tt, xx])
            # e = 0
            u[tt + 1, xx + 1] = (e - (a + b + d)) / (0.5 / dt + 0.5 * c / dx)

def analitic():
    res = np.zeros((nt, nx))
    for tt in range(nt):
        for xx in range(nx):
            time, coord = t[tt], x[xx]
            if 2 * time <= coord:
                res[tt, xx] = time * (coord - time * 0.5) + np.cos(np.pi * (2 * time - coord))
            else:
                res[tt, xx] = time * (coord - time * 0.5) + (2 * time - coord) ** 2 / 8 + np.exp(coord * 0.5 - time)
    return res

nx, nt = 300, 300
L = 1
tp = 0.2
c = 2
pulselen = c * tp

dimx = L + c * tp
dimt = dimx / c
x, dx = np.linspace(0, dimx, nx, retstep=1)
t, dt = np.linspace(0, dimt, nt, retstep=1)

u = np.zeros((nt, nx))

for tt in range(nt):
    if (tt + 1) * dt <= tp:
        u[tt, 0] = 1

for i in range(nx):
    if x[i] > L:
        end = i + 1
        break

transport()
# anal = analitic()

plt.plot(t, u[:, end], 'r', t, u[:, 0], 'b')
plt.show()


