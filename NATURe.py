# -*- coding: utf-8 -*-


import numpy as np

#   Программа предназначена для численного решения трехмерного нестационарного уравнения теплопроводности с коэффициентом теплопроводности, зависящим от температуры
#   Размерность физических единиц соответсвует системе СИ



class Media:
    #   Определение материальных параметров усиливающей среды

    #   Конструктор
    def __init__(self, ro=4560., cp=480., k=lambda T:10.05+40.7*np.exp(-(T-43.8)/42.7), length=2e-3, diametr=5e-3, gamma=280.):
        #   ro - плотность
        #   cp - удельная теплоемкость
        #   k - коэффициент теплопроводности, является функцией
        #   length - длина усиливающей среды
        #   diametr - поперечный размер усиливающей среды
        #   gamma - коэффициент поглощения по накачке / для случая 2,5% Yb:YAG gamma=1.32e2*2.5
        
        self.delta = ro*cp
        self.k = k
        self.length = length
        self.diametr = diametr
        self.gamma = gamma
        print( u'Активный элемент создан с параметрами: толщина %.1f мм, диаметр %1.f мм, теплопроводность К=%.2f при Т=300К' % 
            (length*1e3, diametr*1e3, k(300) ) )



class Solver:
    #   Набор функций для решения уравнения

    #   Конструктор
    def __init__(self, media):
        #   media - экземпляр класса Media
    
        self.media = media

        
    #   Генерация пространственно-временной сетки
    def grid(self, tmin=0, tmax=10, nt=50, nx=40, nz=20):
        #   tmin - начальный момент времени
        #   tmax - конечный момент времени
        #   nt - количество точек по времени
        #   nx - количество точек в поперечном направлении относительно оси накачки
        #   nz - количество точек в продольном направлении относительно оси накачки
    
        self.nt, self.nx, self.nz = nt, nx, nz
        self.x, self.dx = np.linspace(-self.media.diametr*0.5, self.media.diametr*0.5, nx, retstep=1)
        self.z, self.dz = np.linspace(0, self.media.length, nz, retstep=1)
        self.t, self.dt = np.linspace(tmin, tmax, nt, retstep=1)
        print( u'Расчетная область сгенерирована c параметрами: tmax=%.1f, nt=%d, nx=%d, nz=%d' % (tmax,nt,nx,nz) )
        
    
    #   Определение источника нагрева
    def source(self, power=80, radius=350e-6, losses=1-0.94/1.03, shape='hyper', tp='continuous'):
        #   power - мощность источника накачки в Вт
        #   radius - радиус профиля излучения накачки по уровню 1e-2
        #   losses - часть энергии накачки, переходящая в тепло
        #   shape -  пространственная форма профиля излучения, имеет ключевые слова : 'hyper', 'gaussian'
        #   tp - длительность импульса накачки
        
        self.Q = np.zeros((self.nx, self.nx, self.nz))
        self.P = np.zeros((self.nx, self.nx, self.nz))
        self.tp = tp
        distrib = np.zeros((self.nx, self.nx))
        
        for xx in range(self.nx):
            for yy in range(self.nx):
                
                if shape == 'hyper':
                    distrib[xx,yy] = np.exp(-2*((self.x[xx]**2 + self.x[yy]**2)/radius**2)**3)
                elif shape == 'gaussian':
                    distrib[xx,yy] = np.exp(-2*(self.x[xx]**2 + self.x[yy]**2)/radius**2) 
                elif shape == 'fwhm':
                    distrib[xx,yy] = np.exp(-np.log(2)*(self.x[xx]**2+self.x[yy]**2)/(radius**2))
                else:
                    print(u'Неизвестный формат пространственного профиля!')
                    raise NameError
        normalization = np.sum(distrib) * self.dx**2
        for xx in range(self.nx):
            for yy in range(self.nx):
                self.P[xx,yy,:] = power * np.exp(-self.media.gamma*self.z) * distrib[xx,yy] / normalization
                self.Q[xx,yy,:] =  self.P[xx,yy] * losses * self.media.gamma 
                
        convergence = np.sum(self.Q)*self.dx*self.dx*self.dz / (power*losses*(1-np.exp(-self.media.gamma*self.z[-1])))
        print(u'Мощность источника: %.1f Вт, радиус: %.1f мкм, профиль: %s, длительность: %s' % (power, radius*1e6, shape, str(tp)))
        print(u'Проверка сходимости интегральной нормированной мощности источника: %.5f' % convergence)
        
        if tp == 'continuous':
            self.driver = lambda time: 1
        else:
            def driver(self, time):
                if abs(time) <= tp:
                    return 1
                else:
                    0
        

    #   Решение системы алгебраических уравнений с трехдиаганальной матрицей
    def tdma(self, temperature, dh, alpha, beta, Q, points, Tleft, Tright, flowLeft, flowRight):        
        #   temperature - массив значений температуры
        #   dh - шаг по пространственной сетке
        #   alpha, beta - массивы прогоночных коэффициентов
        #   Q - иточник нагрева
        #   points - количество точек
        #   flowLeft - поток на "левой" границе
        #   flowRight - поток на "правой" границе
        #   Tleft - температура на "левой" границе
        #   Tright - температура на "правой" границе
    
        temp = temperature.copy()
        k = self.media.k
        delta = self.media.delta/(self.dt/3.)
        h = dh**2
        
        if flowLeft == False:
            alpha[0] = 0
            beta[0] = Tleft
        else:
            alpha[0] = 1
            beta[0] = -flowLeft*dh/k(temp[0]) 
        
        for i in range(1, points-1): 
            a = k(temp[i])/h
            c = k(temp[i-1])/h
            b = delta + a + c
            d = -temp[i] * delta - Q[i]     
            alpha[i] = -a / (c*alpha[i-1] - b)  
            beta[i] = (d - c*beta[i-1]) / (c*alpha[i-1] - b)
        
        if flowRight != False:
            kv = k(temp[points-1])
            temp[points-1] = (kv*beta[points-2] - flowRight*dh) / (kv*(1-alpha[points-2]))
        else:
            temp[points-1] = Tright
    
        for i in reversed(range(points-1)):
            temp[i] = beta[i] + alpha[i]*temp[i+1] 
        
        return temp


    #   Решение уравнения для всех точек области
    def solver(self, initial='room', bounds={ 'x':{'Tleft':300, 'Tright':300, 'flowLeft':False, 'flowRight':False},
                                              'y':{'Tleft':300, 'Tright':300, 'flowLeft':False, 'flowRight':False},
                                              'z':{'Tleft':False, 'Tright':False, 'flowLeft':-1, 'flowRight':1} }, timeLog=False):
        #   initial - начальное условие для функции температуры, два ключа - 'room'/'cryogenic'
        #   bounds - граничные условия для каждой из осей x,y,z, передаются в формате 
        #   {ось : {температура на левой границе:значение, температура на правой границе:значение, поток на левой границе:значение, поток на правой границе:значение},...}
        #   для границы может использоваться только один тип условий, поэтому для другого типа должно передаваться условие False
        #   timeLog - массив с тремя индексами для записи динамики нагрева в заданной точке
        
        for axis in bounds.keys():
            if bounds[axis]['flowLeft'] == False:
                if bounds[axis]['flowRight'] == False:
                    print(u'Для оси %s заданы условия: температура на левой границе %.1f, температура на правой границе %.1f' % 
                                            (axis, bounds[axis]['Tleft'], bounds[axis]['Tright']))
                else:
                    print(u'Для оси %s заданы условия: температура на левой границе %.1f, поток на правой границе %.1f' % 
                                            (axis, bounds[axis]['Tleft'], bounds[axis]['flowLeft']))
            else:
                if bounds[axis]['flowRight'] == False:
                    print(u'Для оси %s заданы условия: поток на левой границе %.1f, температура на правой границе %.1f' % 
                                            (axis, bounds[axis]['flowLeft'], bounds[axis]['Tright']))
                else:
                    print(u'Для оси %s заданы условия: поток на левой границе %.1f, поток на правой границе %.1f' % 
                                            (axis, bounds[axis]['flowLeft'], bounds[axis]['flowLeft']))
        if initial == 'room':
            self.temperature = np.full((self.nx,self.nx,self.nz), 300)
            print(u'Среда при комнатной температуре')
        else:
            self.temperature = np.full((self.nx,self.nx,self.nz), 100)  
            print(u'Среда при криогенной температуре')
            
        if timeLog == True:
            self.dynamic = np.full(self.nt, 300 if initial == 'room' else 100)  
            print(u'Режим записи динамики нагрева')
                 
        marker = self.nt/10.                
        for tt in range(1,self.nt):
            # x-ось
            Tleft, Tright = bounds['x']['Tleft'], bounds['x']['Tright']
            flowLeft, flowRight = bounds['x']['flowLeft'], bounds['x']['flowRight']
            for zz in range(self.nz):
                for yy in range(self.nx):
                    self.temperature[:,yy,zz] = self.tdma(temperature=self.temperature[:,yy,zz], Q=self.Q[:,yy,zz]*self.driver(self.t[tt]),
                                                          dh=self.dx, points=self.nx,
                                                          alpha=np.zeros(self.nx-1), beta=np.zeros(self.nx-1),
                                                          Tleft=Tleft, Tright=Tright, flowLeft=flowLeft, flowRight=flowRight)
            # y-ось
            Tleft, Tright = bounds['y']['Tleft'], bounds['y']['Tright']
            flowLeft, flowRight = bounds['y']['flowLeft'], bounds['y']['flowRight']
            for zz in range(self.nz):
                for xx in range(self.nx):
                    self.temperature[xx,:,zz] = self.tdma(temperature=self.temperature[xx,:,zz], Q=self.Q[xx,:,zz]*self.driver(self.t[tt]),
                                                          dh=self.dx, points=self.nx,
                                                          alpha=np.zeros(self.nx-1), beta=np.zeros(self.nx-1),
                                                          Tleft=Tleft, Tright=Tright, flowLeft=flowLeft, flowRight=flowRight)        
            # z-ось
            Tleft, Tright = bounds['z']['Tleft'], bounds['z']['Tright']
            flowLeft, flowRight = bounds['z']['flowLeft'], bounds['z']['flowRight']
            for xx in range(self.nx):
                for yy in range(self.nx):
                    self.temperature[xx,yy,:] = self.tdma(temperature=self.temperature[xx,yy,:], Q=self.Q[xx,yy,:]*self.driver(self.t[tt]),
                                                          dh=self.dz, points=self.nz,
                                                          alpha=np.zeros(self.nz-1), beta=np.zeros(self.nz-1),
                                                          Tleft=Tleft, Tright=Tright, flowLeft=flowLeft, flowRight=flowRight)
            if timeLog == True:
                self.dynamic[tt] = self.temperature[self.nx/2,self.nx/2,0]
                
            if tt/marker in range(1,11):
                print(u'Выполнено: %.1f %%' % (100.*tt/self.nt))
        
        
    #   Визуализация результатов моделирования
    def show(self, key='steady'):
        #   key - 'dynamic'/'steady'

        import matplotlib.pyplot as plt
        plt.rc('font', family='Verdana')
        
        if key == 'dynamic':
            plt.plot(self.t, self.dynamic, label=u'Динамика нагрева активного элемента')
            plt.xlabel(u'Время, с')
            plt.ylabel(u'Температура, К')
            plt.grid()
            plt.legend(loc=4)
            plt.show()
        else:
            plt.imshow(self.temperature[:,:,0])
            plt.colorbar()
            plt.show()
                
                
                
                
        
if __name__ == '__main__':
    media = Media()
    solver = Solver(media)
    solver.grid(tmax=5, nt=80, nx=30, nz=20)
    solver.source(shape='hyper')
    solver.solver(timeLog=True)
    solver.show('steady')
    
