# -*- coding: utf-8 -*-

#   Программа предназначена для численного решения трехмерного нестационарного уравнения теплопроводности с коэффициентом теплопроводности, зависящим от температуры
#   Размерность физических единиц соответсвует системе СИ


import numpy as np
import matplotlib.pyplot as plt

import sys
import solvers
from solvers import conductivity, TDMA, TDMAME, Inversion, Square

class Heat:
    #   Набор функций для решения уравнения

    #   Конструктор
    def __init__(self, media, pump, verbose=False):
        #   media - список экземпляров класса Media
        #   pump - кземляр класса Pump
    
        self.media = media
        self.pump = pump
        self.quants = len(media)
        self.verbose = verbose

        
    #   Генерация пространственно-временной сетки
    #   Количество точек на элемент однинаковое
    def grid(self, tmin=0, tmax=10, nt=50, nx=40, nz=20):
        #   tmin - начальный момент времени
        #   tmax - конечный момент времени
        #   nt - количество точек по времени
        #   nx - количество точек в поперечном направлении относительно оси накачки
        #   nz - количество точек в продольном направлении относительно оси накачки
    
        self.nt, self.nx, self.nz = nt, nx, nz
        self.Nz = self.nz * self.quants
        self.t, self.dt = np.linspace(tmin, tmax, nt, retstep=1)
        self.dt /= 3.
        self.x, self.dx = np.linspace(-self.media[0].diameter * 0.5, self.media[0].diameter * 0.5, nx, retstep=1)
        self.deltas = np.zeros(self.quants)
        self.steps = np.zeros(self.quants)
        for el in range(self.quants):
            elem = self.media[el]
            self.deltas[el] = elem.cp * elem.ro / self.dt
            elem.z, self.steps[el] = np.linspace(0, elem.length, nz, retstep=1)
        if self.verbose:
            print( u'Расчетная область сгенерирована c параметрами: tmax=%.1f, nt=%d, nx=%d, nz=%d' % (tmax, nt, nx, nz) )
        
    
    #   Определение источника нагрева
    def source(self):
        #   losses - часть энергии накачки, переходящая в тепло
        
        self.Q = np.zeros((self.nx, self.nx, self.Nz))
        self.P = np.zeros((self.nx, self.nx, self.Nz))
        self.pump.grid(x=self.x, dx=self.dx, nx=self.nx, verbose=self.verbose)


    def cryomech(self, P, links=True):

        if links:
            return 38.3 + 0.18 * P + 5.88e-3 * P ** 2
        else:
            P *= 0.5
            return 38.3 + 0.18 * P + 5.88e-3 * P ** 2


    def phase_aberrations(self, timepoint, where='space'):

        #   Фаза во времени
        thermal_average = np.zeros((self.nx, self.nx, self.nz))
        if where == 'time':
            for xx in range(self.nx):
                for yy in range(self.nx):
                    if abs(self.x[xx]) < self.pump.radius and abs(self.x[yy]) < self.pump.radius:
                        for zz in range(self.nz):
                            thermal_average[xx, yy, zz] = self.temperature[xx, yy, zz] - self.temperature[self.nx - 1, self.nx - 1, zz]
            self.thermalPhaseTime[timepoint] = np.sum(thermal_average) * self.average_coefficient

        #   Фаза в пространстве
        elif where == 'space':
            self.thermalPhaseSpatial = np.zeros((self.nx, self.nx))
            for xx in range(self.nx):
                for yy in range(self.nx):
                    temperature_difference = self.temperature[xx, yy, :self.nz] - self.temperature[self.nx - 1, self.nx - 1, :self.nz]
                    self.thermalPhaseSpatial[xx, yy] = np.sum(temperature_difference) * self.average_coefficient


    #   Решение уравнения для всех точек области
    def solver(self, initial='cryogenic', cooling_axis='z', links=True, timeLog=False, aberrations=False):
        #   initial :: str - начальное условие для функции температуры, два ключа - 'room'/'cryogenic'
        #   cooling_axis :: str - ось, вдоль которой происходит охлаждение; 'x'/'y'/'z'
        #   links :: bool - охлаждение с линками или без
        #   timeLog :: bool
        #   aberrations :: str - расчет тепловой составляющей фазовых искажений; 'time' / 'space'

        if initial == 'room':
            self.temperature = np.full((self.nx, self.nx, self.Nz), 300.)
            print(u'Среда при комнатной температуре')
        else:
            self.temperature = np.full((self.nx, self.nx, self.Nz), 90.)
            print(u'Среда при криогенной температуре')

        if timeLog:
            self.dynamic = np.full(self.nt, 300. if initial == 'room' else 90.)
            print(u'Режим записи динамики нагрева')

        if aberrations:

            if aberrations == 'time':
                self.thermalPhaseTime = np.zeros(self.nt)
                self.average_coefficient = self.steps[0] * self.dx ** 2 / ((2 * self.pump.radius) ** 2) * self.media[0].xi / self.media[0].ls
            elif aberrations == 'space':
                self.average_coefficient = self.steps[0] * self.media[0].xi / self.media[0].ls
                self.thermalPhaseSpatial = np.zeros((self.nx, self.nx))

        n = np.zeros((self.nx, self.nx, self.nz))
        conc = self.media[0].concentration
        hv = self.media[0].h * self.media[0].freqp
        life = self.media[0].lifetime
        gamma = np.zeros(self.nz)
        losses = self.media[0].losses
        dl = self.steps[0]
        pump_points = self.pump.points_per_pumping
        pumping_time = self.pump.pumping_time
        period = self.pump.period
        pulse_duration = self.pump.tp
        repetition_rate_coeff = min(1, pulse_duration / period)
        self.media[1].bounds[cooling_axis]['Tright'] = self.cryomech(P=self.pump.power * repetition_rate_coeff, links=links)
        print(u'Расчет начат')
        for tt in range(self.nt):

            for xx in range(self.nx):
                for yy in range(self.nx):
                    for zz in range(self.nz):
                        sigma = solvers.ACS(T=self.temperature[xx, yy, zz])
                        amplitude = self.P[xx, yy, zz] * sigma / hv
                        f01 = solvers.lower_manifold_pump(T=self.temperature[xx, yy, zz])
                        f12 = solvers.upper_manifold_pump(T=self.temperature[xx, yy, zz])
                        n[xx, yy, zz] = conc - Inversion(points=pump_points, x0=0, xn=pumping_time, init=0,
                                                         amplitude_main=amplitude, concentration=conc, lifetime=life,
                                                         f01=f01, f12=f12, period=period, pulse_duration=pulse_duration)
                        gamma[zz] = n[xx, yy, zz] * sigma * f01
                        self.P[xx, yy, zz] = self.pump.power_distribution[xx, yy] * np.exp(-sum(gamma[0:zz]) * dl) * repetition_rate_coeff
                    self.Q[xx, yy, 0:self.nz - 1] = - np.diff(self.P[xx, yy, 0:self.nz]) / dl * losses / 3.

            for el in range(self.quants):
                elem = self.media[el]
                # x-ось
                Tleft, Tright = elem.bounds['x']['Tleft'], elem.bounds['x']['Tright']
                flowLeft, flowRight = elem.bounds['x']['flowLeft'], elem.bounds['x']['flowRight']
                for zz in range(self.nz * el, self.nz * (1 + el)):
                    for yy in range(self.nx):
                        self.temperature[:, yy, zz] = TDMA(points=self.nx, dh=self.dx, delta=self.deltas[el],
                                                           temperature=self.temperature[:, yy, zz],
                                                           Q=self.Q[:, yy, zz], material=el,
                                                           Tleft=Tleft, Tright=Tright,
                                                           flowLeft=flowLeft, flowRight=flowRight)
                # y-ось
                Tleft, Tright = elem.bounds['y']['Tleft'], elem.bounds['y']['Tright']
                flowLeft, flowRight = elem.bounds['y']['flowLeft'], elem.bounds['y']['flowRight']
                for zz in range(self.nz * el, self.nz * (1 + el)):
                    for xx in range(self.nx):
                        self.temperature[xx, :, zz] = TDMA(points=self.nx, dh=self.dx, delta=self.deltas[el],
                                                           temperature=self.temperature[xx, :, zz],
                                                           Q=self.Q[xx, :, zz], material=el,
                                                           Tleft=Tleft, Tright=Tright,
                                                           flowLeft=flowLeft, flowRight=flowRight)
            # z-ось
            Tleft, Tright = self.media[0].bounds['z']['Tleft'], self.media[-1].bounds['z']['Tright']
            flowLeft, flowRight = self.media[0].bounds['z']['flowLeft'], self.media[-1].bounds['z']['flowRight']
            for xx in range(self.nx):
                for yy in range(self.nx):
                    self.temperature[xx, yy, :] = TDMAME(points=self.nz, dh=self.steps, Delta=self.deltas,
                                                       temperature=self.temperature[xx, yy, :],
                                                       Q=self.Q[xx, yy, :],
                                                       Tleft=Tleft, Tright=Tright,
                                                       flowLeft=flowLeft, flowRight=flowRight)
            if timeLog:
                self.dynamic[tt] = self.temperature[self.nx/2, self.nx/2, 0]

            if aberrations == 'time':
                self.phase_aberrations(timepoint=tt, where=aberrations)

            sys.stdout.write(u'Выполнено: %.1f %%' % (100. * (tt + 1) / self.nt))
            sys.stdout.write('\r')

        if aberrations == 'space':
            self.phase_aberrations(timepoint=tt, where=aberrations)

        print(u'Расчет окончен')
        
    #   Визуализация результатов моделирования
    def show(self, key='steady'):
        #   key - 'dynamic'/'steady'


        plt.rc('font', family='Verdana')
        
        if key == 'dynamic':
            plt.plot(self.t, self.dynamic, label=u'Динамика нагрева активного элемента')
            plt.xlabel(u'Время, с')
            plt.ylabel(u'Температура, К')
            plt.grid()
            plt.legend(loc=4)
            plt.show()
        else:
            plt.imshow(self.temperature[:,:,0])
            plt.colorbar()
            plt.show()


if __name__ == '__main__':

    crystal = solvers.Media(length=0.375, diameter=2.5, concentration=10)
    cooler = solvers.Media(length=5, diameter=2.5, cp=0.4, ro=8.8)
    pump = solvers.Pump(radius=1e-1, power=200, tp=0.5e-3, period=1e-3, points_per_pumping=250, pumping_time=1.1e-3)
    insular = 1e-9
    crystal.Bounds({ 'x':{'Tleft':False, 'Tright':False, 'flowLeft':insular, 'flowRight':insular},
                    'y':{'Tleft':False, 'Tright':False, 'flowLeft':insular, 'flowRight':insular},
                    'z':{'Tleft':False, 'Tright':False, 'flowLeft':insular, 'flowRight':insular}})
    cooler.Bounds({'x': {'Tleft': False, 'Tright': False, 'flowLeft': insular, 'flowRight':insular},
                    'y': {'Tleft': False, 'Tright': False, 'flowLeft': insular, 'flowRight': insular},
                    'z': {'Tleft': False, 'Tright': False, 'flowLeft': insular, 'flowRight': 0}})
    solver = Heat(media=[crystal, cooler], pump=pump)
    solver.grid(tmax=100, nt=100, nx=20, nz=30)
    solver.source()
    solver.solver(initial='cryo', cooling_axis='z', links=True, timeLog=False, aberrations='space')
    # axis = [0, crystal.length + cooler.length, solver.x[0], solver.x[-1]]
    # plt.imshow(solver.temperature[:, solver.nx/2, :], extent=axis)
    # plt.colorbar()
    # plt.show()
    plt.plot(solver.temperature[int(solver.nx/2), int(solver.nx/2)])
    plt.show()
    # plt.imshow(solver.thermalPhaseSpatial)
    # plt.colorbar()
    # plt.show()

